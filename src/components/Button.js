import "./css/Button.css";
import { Component } from 'react'


// const Button = function (props) {
//   const { onClickHandler, value } = props;
//   return (
//     <button className="btn" onClick={onClickHandler}>
//       {value}
//     </button>

//   );
//};

class Button extends Component {
  // constructor(props){
  //   super(props)
  // }

  render() {
    const { onClickHandler, value,className } = this.props;

    return (
      <button className={'btn '+ className} onClick={onClickHandler}>
        {value}
      </button>
    );
  }
}

export default Button;
