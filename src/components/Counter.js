//import React, { useState } from 'react'
import './css/Counter.css'
import Button from './Button'

const Counter =  ({ value, uid, items, setItems }) => {

    // const [qty, setQty] = useState(value);

    // const qtyButtonAdd=()=>setQty(qty+1);
    // const qtyButtonSub=()=>setQty(qty-1>0?qty-1:0);
    // const qtyInputChange=(ev)=>{
    //     let qty=parseInt(ev.target.value);
    //     if(qty<0|| isNaN(qty)){
    //         qty=0;
    //     }
    //     setQty(qty);
    // }

    const qty = value;
    const qtyButtonAdd = () => {
        let newItems = items.slice(); //[...items];
        let index = newItems.findIndex(item => item.uid === uid)
        newItems[index].qty++;
        setItems(newItems);
    }
    const qtyButtonSub = (ev) => {
        let newItems = items.slice(); //[...items];
        let index = newItems.findIndex(item => item.uid === uid)
        newItems[index].qty = parseInt(ev.target.value);
        if (newItems[index].qty < 0 || isNaN(newItems[index].qty)) {
            newItems[index].qty = 0;
        }
        setItems(newItems);
    }
    const qtyInputChange = () => {
        let newItems = items.slice(); //[...items];
        let index = newItems.findIndex(item => item.uid === uid)
        newItems[index].qty--;
        if (newItems[index].qty < 0 || isNaN(newItems[index].qty)) {
            newItems[index].qty = 0;
        }
        setItems(newItems);
    }
    return (
        <div className='Counter'>
            <div className='Counter_info'>
                <Button
                    value="-"
                    onClickHandler={qtyButtonSub}
                // onClickHandler={() => {
                //     setQty(qty - 1);
                //     //alert("-" + uid);
                // }}
                />
                <input className='Counter_input'
                    value={qty}
                    onChange={qtyInputChange}
                // onChange={(ev) => {
                //     console.log(ev.target.value);
                //     setQty(parseInt(ev.target.value));
                // }}
                />
                <Button
                    value="+"
                    onClickHandler={qtyButtonAdd}
                // onClickHandler={() => {
                //     //alert("+" + uid);
                //     setQty(qty + 1);

                // }}
                />

            </div>
        </div>
    )
}
export default Counter
