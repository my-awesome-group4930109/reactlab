import React, { useState } from "react";
import './css/Basket.css'
import { themes, ThemeContext } from "../contexts/theme-context";

import BasketHeader from './BasketHeader'
import BasketItem from './BasketItem'
import BasketPromoCode from './BasketPromoCode'
import BasketPromoInfo from './BasketPromoInfo'
import BasketTotal from './BasketTotal'
import Button from './Button'

const Basket = (props) => {

  const [items, setItems] = useState(props.items);

  const countItemsInBasket = items.reduce((acc, next) => acc + next.qty, 0);
  const amountTotal = items.reduce((acc, next) => acc + next.price * next.qty, 0);

  return (<ThemeContext.Consumer>
    {({ theme, setTheme }) => {

      return <div className={"Basket Basket-" + theme}>
        <BasketHeader count={countItemsInBasket} />

        <div className="Basket__items">

          {items.map(item => (
            <BasketItem {...item} key={item.uid} items={items} setItems={setItems} />
          ))}

          <BasketPromoInfo code={'REACTSPECIALIST'} />
          <BasketTotal value={amountTotal} currency={'руб'} />
          <BasketPromoCode code={''} />

          <Button value='Продолжить покупку'
            onClickHandler={() => { alert('Продолжить') }}
            className="btn-proceed" />

          <Button value='Темная тема'
            onClickHandler={() => { setTheme(themes.dark) }}
          />
          <Button value='Светлая тема'
            onClickHandler={() => { setTheme(themes.ligth) }}
          />
          <Button value='Отладка тема'
            onClickHandler={() => { setTheme(themes.debug) }}
          />
        </div>
      </div>;
    }}
  </ThemeContext.Consumer>
  );
}
export default Basket;
