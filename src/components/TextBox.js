import './TextBox.css';
 const TextBox = function (props) {
       const { value, type,onChangeHandler } = props;
       return (
         <input className="txt" type={type} value={value} onChange={onChangeHandler}>
           
         </input>
    
       );
    };
    export default TextBox;