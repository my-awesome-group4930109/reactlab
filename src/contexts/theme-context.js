import React from "react";

export const themes={
    ligth: "ligth",
    dark: "dark",
    debug: "debug"
}

export const ThemeContext=React.createContext({
    theme:themes.debug
})