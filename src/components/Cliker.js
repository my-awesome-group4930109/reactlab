//import { render } from '@testing-library/react';
import React, { useState } from 'react'
//В виде класса
// class Clicker extends React.Component {
//     constructor(props) {
//         super(props);

//         this.state = {
//             num: props.value,
//             isDark: false
//         }
//         this.clickHandler = this.clickHandler.bind(this);

//     }

//     clickHandler(ev){
//       const newState={
//             num: this.state.num + 1,
//             isDark: !this.state.isDark
//         }

//         this.setState(newState)
//         console.log(newState)
//     }

//     render() {
//         return (
//             <button
//                 className={'btn ' + (this.state.isDark ? 'btn-dark' : '')}
//                 onClick={this.clickHandler}
//             >
//                 Clicker {this.state.num}
//             </button>
//         )
//     }
// }

//в виде функционального компанента

 const Clicker = ({ value }) => {

     const [num, setNum] = useState(value)
     const [isDark, setIsDark] = useState(false)

     const clikHandler = ev => {
         setNum(num + 1);
         setIsDark(!isDark);
     }

     return (
         <button
             className={'btn ' + (isDark ? 'btn-dark' : '')}
             onClick={clikHandler}
         >
             Clicker {num}
         </button>
     )
 }
export default Clicker

Clicker.defaultProps = {
    value: 0
}