import "./App.css";
import { useState, useEffect } from "react";
import { themes, ThemeContext } from "./contexts/theme-context";
import CatalogPage from "./components/CatalogPage";

import Basket from "./components/Basket.js";
//import Clicker from "./components/Cliker";
import Modal from "./components/Modal";
import Button from "./components/Button";

//import Wrapper from "./components/Wrapper.js"

function App() {
  const [startItems, setStartItems] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  const [theme, setTheme] = useState(themes.light);
  const [currentPach, setCurrentPach] = useState('/');

  const [isShow, setIsShow] = useState(false);
  const [name, setName] = useState('Гость');

  const clickTopMenuHandler = ev => {
    ev.preventDefault();
    let path = ev.target.getAttribute('data-route');
    console.log(`нажали для перехода ${path}`)

    const state = {}
    const title = ''
    window.history.pushState(state, title, path)
    setCurrentPach(path);
  }

  useEffect(() => {
    fetch("http://localhost:3000/items.json")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setStartItems(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  let basketPlace = null;
  if (error) {
    basketPlace = <div>Ошибка:{error.message}</div>
  } else if (!isLoaded) {
    basketPlace = <div>Загрузка...</div>
  } else {
    basketPlace = <Basket items={startItems} />
  }



  // const [txt, setTxt] = useState('');

  basketPlace = <ThemeContext.Provider value={{ theme, setTheme }}>
    {basketPlace}
  </ThemeContext.Provider>

  let title = null;
  let content = null;

  switch (currentPach) {
    case '/':
      title = 'Каталог товаров';
      content = <CatalogPage/>//<div>Тут может быть перечень товаров</div>
      break;
    case '/basket':
      title = 'Корзина';
      content = basketPlace
      break;
    case '/about':
      title = 'О нас';
      content = <div>информация о компании</div>
      break;
    case '/react-router':
      title = 'React Router';
      content = <div>Часто используются не самописные, а сторонние системы рутинга,
        <a href="https://reactrouter.com/" target="_blank">React Router</a></div>
      break;
    default:
      break;
  }

  const modal = <Modal title={"Модальное окно"}
    buttons={[
      <Button
        key={1}
        value="Закрыть"
        onClickHandler={() => setIsShow(false)}
      />,
      <Button
        key={2}
        value="Кнопка"
        
        onClickHandler={() => alert("Кнопка")}
      />
    ]}
  >
    <div>
      <p>Это диалоговое окно</p>
      <input
        value={name}
        onChange={ev => setName(ev.target.value)}
        className="Modal-input-text"
      />
    </div>
  </Modal>
  return (
    <div className="App">


      {isShow ? modal : null}

      <header className="App-header">

        <Button
          value="Показать модальное окно"
          onClickHandler={() => setIsShow(true)}
        />

        {/* <input value={txt} onChange={ev => setTxt(ev.target.value)} />
        <h2>{txt}</h2>
        <h2>{txt}</h2>
        <h2>{txt}</h2>
        <h2>{txt}</h2>        
        <h2>Длина:{txt.length}</h2> */}

        {/* <Wrapper> */}

        <nav className="App-nav">
          <ul>
            <li>
              <a href="#" onClick={clickTopMenuHandler} data-route={'/'}>Главная</a>
            </li>
            <li>
              <a href="#" onClick={clickTopMenuHandler} data-route={'/basket'}>Корзина</a>
            </li>
            <li>
              <a href="#" onClick={clickTopMenuHandler} data-route={'/about'}>О нас</a>
            </li>
            <li>
              <a href="#" onClick={clickTopMenuHandler} data-route={'/react-router'}>О рутинге</a>
            </li>
            <li>
              <a>{name}</a>
            </li>
          </ul>
        </nav>
        <h1>{title}:{name}</h1>
        {content}

        {/* </Wrapper>
        <Wrapper color="green"> */}
        {/* <Basket items={items} /> */}
        {/* </Wrapper> */}

        {/* <Clicker/>
        <Clicker value={3}/> */}

      </header>
    </div>
  );
}

export default App;
