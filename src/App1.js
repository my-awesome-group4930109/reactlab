import "./App.css";
import Button from "./components/Button";
import TextBox from "./components/TextBox";

function Wrapper(props){
  return(
    <blockquote>
      {props.children}
    </blockquote>
  )
}

function App() {
  //const css = { color: "#336699", textTransform: "uppercase" };
  /*const result = true ? <strong>1</strong> : null;
  const exp1 = (
    <div className="some" title="jlkjlk">
      <em>2 + 3 = {2 + 3}</em>
    </div>
  );
  const date = new Date();
  const odd = <div>нечетный</div>;
  const even = <div>четный</div>;
  const result1 = date.getHours() % 2 ? odd : even;*/
  const imgeURL = `https://placekitten.com/100/100`;
  const image = <img src={imgeURL} alt="btn"></img>;

  // function Hello(props) {
  //   return (
  //     <div>
  //       <h1>Привет, {props.name}</h1>
  //       <p>Количество: {props.qty}</p>
  //     </div>
  //   );
  // }
  //стрелочная
  const Hello = (props) => (
    <div>
      <h1>Привет, {props.name}</h1>
      <p>Количество: {props.qty}</p>
      <button>{props.bb}</button>
    </div>
  );

  const buttons = [
    { value: "Кнопка1", fn: () => console.log(1) },
    { value: "Кнопка2", fn: () => console.log(2) },
    { value: "Кнопка3", fn: () => console.log(3) },
    { value: "Кнопка4", fn: () => console.log(4) },
  ];

  return (
    <div>
      {/* <h1 title="Привет" className={"foo"} style={css}>
        Привет, мир!!
      </h1> */}
      <Hello name={"мир"} qty={4} bb={"Save"} />
      <br />
      {image} <br />
      {/*{new Date().toLocaleTimeString()}
      <br />
       {23 + 6}
      <br />
      {"12" + "jkjklj"}
      <br />
      {result}

      {exp1}

      {date.toLocaleTimeString()}
      {result1} */}
      <div>
        <Button value={"Моя кнопка!"} onClickHandler={() => alert("Ура!")} />
        <Button value={"Кнопка2"} onClickHandler={() => alert("Кнопка2!")} />
        <Button value={"Кнопка3"} onClickHandler={() => alert("Кнопка3!")} />

        <hr />

        <Wrapper>
          {buttons.map((button, index) => (
            <Button
              value={button.value}
              onClickHandler={button.fn}
              key={index}
            />
          ))}
        </Wrapper>
      </div>


      
      <hr />
      <div>
        <TextBox
          value="какой то текст"
          type="text"
          onChangeHandler={() => alert("изменен")}
        ></TextBox>
      </div>
    </div>
  );
}

export default App;
